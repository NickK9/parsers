import json
from spreadsheet import Spreadsheet


categorys = ['art', 
    'cosplay', 
    'comics', 
    'writing', 
    'photography', 
    'podcast', 
    'blogging', 
    'gaming', 
    'design', 
    'advice', 
    'animation', 
    'commissions', 
    'dance theatre', 
    'drawing painting', 
    'education', 
    'food drink', 
    'fundraising', 
    'health fitness', 
    'lifestyle', 
    'money', 
    'music', 
    'science tech', 
    'social', 
    'software', 
    'streaming', 
    'videoandfilm']

users_dic = {}
uniq_users = []
users_categories = {}
for c in categorys:
    uniq = []
    with open("ko-fi_gold_users_{}.json".format(c), "r") as f:
        users = json.load(f)
    users_dic[c] = users
for i in users_dic:
    for j in users_dic[i]:
        uniq_users.append(j)

uniq_users = list(set(uniq_users))

u_cats = {} # нейм юзера со всеми принадлежащими ему категориями
for u in uniq_users:
    u_categories = []
    for p in users_dic:
        for z in users_dic[p]:
            if u == z:
                u_categories.append(p)
    u_cats[u] = u_categories
    
csv_headers = [['username', 'categories', 'ko-fi username', 'instagram', 'twitter', 'reddit', 'youtube', 'facebook', 'self site']]
each_links = {}
for un in uniq_users:
    for i in users_dic:
        for fuck in users_dic[i]:
            each_links[fuck] = users_dic[i][fuck] # все в одной с линками

my_pretty_dic = {} # юзер, категории, ссылки
for i in each_links:
    ended_list = []
    for j in u_cats:
        if i == j:
            ended_list.append(u_cats[j])
            ended_list.append(each_links[i])
            my_pretty_dic[i] = ended_list

#print(len(my_pretty_dic))

finished = []

for i in my_pretty_dic:
    user_d = {'username':'', 'categories':'', 'ko-fi username':'',
        'instagram':'', 'twitter':'', 'reddit':'', 'youtube':'',
        'facebook':'', 'self site':''}
    user_d['categories'] = my_pretty_dic[i][0]
    user_d['ko-fi username'] = i
    other_links = []
    usn = False
    for j in my_pretty_dic[i][1]:
        if 'instagram' in j:
            if not usn:
                un = j.split('instagram.com/')
                user_d['username'] = un[1]
                usn = True
            user_d['instagram'] = j
        elif 'youtube' in j:
            if not usn:
                un = j.split('youtube.com/')
                user_d['username'] = un[1]
                usn = True
            user_d['youtube'] = j
        elif 'reddit' in j:
            if not usn:
                un = j.split('reddit.com/')
                user_d['username'] = un[1]
                usn = True
            user_d['reddit'] = j
        elif 'twitter' in j:
            if not usn:
                un = j.split('twitter.com/')
                user_d['username'] = un[1]
                usn = True
            user_d['twitter'] = j
        elif 'facebook' in j:
            if not usn:
                un = j.split('facebook.com/')
                user_d['username'] = un[1]
                usn = True
            user_d['facebook'] = j
        elif 'http' in j:
            other_links.append(j)
        else:
            pass
    user_d['self site'] = other_links
    user = [user_d['username'], ', '.join(user_d['categories']), user_d['ko-fi username'], 
    user_d['instagram'], user_d['twitter'], user_d['reddit'], user_d['youtube'], 
    user_d['facebook'], ', '.join(user_d['self site'])]
    finished.append(user)

csv_headers += finished

spreadsheet = Spreadsheet('1-GjTbE46XY-2tUmpeOmeQJOucGybriCzieATdpioCwM', 'KoFiGold!A:Q')
spreadsheet.insert_values(csv_headers)

with open('ko-fi_gold_users.csv', "w", newline="") as file:
    writer = csv.writer(file)
    writer.writerows(csv_headers)