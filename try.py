a = '''art, drawing painting
art, design, drawing painting
art, comics, commissions
art, photography, fundraising
art, music
art, comics, commissions
art, comics, commissions
art, design, advice, drawing painting
art, comics, gaming, design, animation, commissions, drawing painting
art, animation, commissions
art, music
art, comics, design, commissions, drawing painting
art, design, animation
art, comics, drawing painting
art, writing, commissions
art, comics, writing, design, social
art, cosplay, comics, photography
art, cosplay, photography, design
art, writing, blogging, design, lifestyle, videoandfilm
art, comics, drawing painting
art, commissions, drawing painting
art, writing, design, drawing painting, social
art, comics, writing, drawing painting
art, commissions, drawing painting
art, drawing painting
art, comics, design, animation, drawing painting, videoandfilm
art, commissions, drawing painting
art, gaming, commissions, streaming
art, photography, gaming
art, animation, commissions
art, drawing painting
art, writing, podcast, blogging, design, commissions, dance theatre, drawing painting, food drink, videoandfilm
art, cosplay, comics, design, videoandfilm
art, gaming, animation, videoandfilm
art, commissions, drawing painting
art
art, blogging, drawing painting
art, comics, drawing painting
art, writing, podcast, blogging, advice, drawing painting, lifestyle, videoandfilm
art, writing
art, gaming, food drink
art, comics
art, drawing painting
art, comics, drawing painting
art, writing, photography
art, comics, drawing painting
art, writing, blogging, design, advice, commissions, drawing painting, health fitness
art, comics, writing, podcast, advice, lifestyle, streaming, videoandfilm
art, comics, commissions
art, photography, podcast
art, cosplay, photography, social
art, cosplay, comics, blogging, commissions
art, writing, podcast
art, commissions, drawing painting
art, writing
art, photography, blogging, design, advice, drawing painting, education, fundraising, lifestyle, social, software, streaming, videoandfilm
art, photography, design, animation, drawing painting, music, science tech, videoandfilm
art, comics
art, comics, design, drawing painting
art, writing
art, education
art, comics, commissions, drawing painting, education, fundraising
art, commissions, drawing painting
art, comics
art, drawing painting, fundraising
art, cosplay
art, cosplay, design
art, design, drawing painting, social
art, comics, writing
art, cosplay, drawing painting
art, commissions, drawing painting
art, comics, commissions, drawing painting
art, comics, writing, gaming, design, animation, commissions, drawing painting, streaming
art
art, comics, drawing painting
art, commissions
art, advice, animation
art, drawing painting
art, comics, drawing painting
art, comics, design, commissions, drawing painting
art, cosplay
art, comics, commissions, drawing painting
art, blogging, design, music
art
art, drawing painting
art, blogging, gaming
art, design, drawing painting, lifestyle
art
art, writing, commissions
art, commissions, drawing painting
art, comics, commissions
art, commissions, drawing painting
art, design, commissions
art, comics, drawing painting
cosplay
cosplay, podcast
cosplay
cosplay
cosplay
cosplay, design
cosplay
cosplay, gaming
cosplay
cosplay
cosplay, design, education
cosplay
cosplay, photography
cosplay
cosplay, comics, gaming, design, commissions, drawing painting, videoandfilm
comics
comics, writing, gaming, drawing painting, streaming
comics, drawing painting
comics, animation, commissions
comics
comics
comics
comics, writing, drawing painting
comics
comics, photography, design, animation, commissions, dance theatre, drawing painting, food drink, health fitness, streaming
comics
comics, drawing painting
comics
comics
comics, commissions
comics, writing
comics, blogging, design, animation, drawing painting, education, science tech, videoandfilm
comics
comics, drawing painting
writing, podcast, education
writing, blogging
writing, dance theatre, music
writing, gaming, advice, streaming
writing, blogging
writing, podcast, blogging, streaming
writing, podcast, gaming, streaming, videoandfilm
writing, blogging
writing
writing, gaming, streaming
writing, fundraising, social
writing, blogging, advice
writing, blogging, advice, lifestyle
writing, blogging, education
writing, education
writing, podcast, advice
writing, photography, blogging, health fitness, videoandfilm
writing, blogging, science tech
writing, advice
writing, podcast
writing
writing
writing
writing, commissions
writing, photography
writing, blogging, education
writing, blogging, food drink, fundraising
writing, drawing painting, education
writing, podcast, blogging
writing, blogging, gaming, education, science tech, social, software
writing, fundraising, science tech
writing, blogging, advice, commissions, education, fundraising, lifestyle, social
writing, blogging, fundraising
writing, podcast, blogging, gaming, design, fundraising, social
writing, blogging
writing, photography, social
writing, gaming, videoandfilm
writing, podcast
writing
writing, videoandfilm
photography, blogging, lifestyle, social
photography, blogging, food drink, lifestyle, social, videoandfilm
photography
photography, science tech
photography, blogging, lifestyle
photography, science tech, social
photography, blogging, education
photography, blogging, music
photography, podcast
photography, science tech, social
photography, blogging, science tech
photography, blogging, design, education, food drink, health fitness, lifestyle, social
photography, animation, commissions, drawing painting, education, social
podcast, commissions, videoandfilm
podcast
podcast, gaming
podcast
podcast, gaming, streaming
podcast
podcast, social, videoandfilm
podcast, blogging
podcast, music, social
podcast
podcast, gaming, videoandfilm
podcast, blogging, streaming
podcast
podcast
podcast, music, streaming
podcast, gaming
blogging, education, social
blogging, gaming, social
blogging, design
blogging, education
blogging, lifestyle, videoandfilm
blogging, advice
blogging, education, software
blogging, gaming, design
blogging, gaming, streaming
blogging, gaming, music
blogging, science tech, social
blogging
blogging
blogging, advice, education
blogging, drawing painting
blogging, commissions
gaming, streaming
gaming, social, software
gaming, science tech, software, videoandfilm
gaming, software
design, education, software
design, streaming
design, drawing painting
design, commissions, drawing painting
design, social, software
design, drawing painting, education
design, commissions, drawing painting
design, science tech, social
design, drawing painting
advice, education, videoandfilm
animation
animation, fundraising
animation
commissions, music, videoandfilm
commissions, drawing painting
drawing painting
drawing painting, science tech
drawing painting, fundraising, social
drawing painting, videoandfilm
drawing painting, fundraising
drawing painting
drawing painting, social
education, music, videoandfilm
education
education, fundraising, science tech
education, videoandfilm
education, fundraising, social
fundraising
fundraising, music
lifestyle
lifestyle
music
music, videoandfilm
music, software
music, social
music, streaming
music
music
science tech, software
software
software
software
software
streaming
streaming
videoandfilm'''

b = a.split('\n')

c = [i.split(',') for i in b]

d = []

for i in c:
    if type(i) is list:
        for j in i:
            d.append(j.strip())
    else:
        d.append(i.strip())

# print(len(d))
# print(len(set(d)))

categories = [i for i in set(d)]

print(categories)

BUY_ME_A_COFFEE = ['writing', 'education', 'journalism', 'design', 'covid-19', 'blog', 'newsletter', 'piano', 'filmmaking', 'food', 'teaching', 'standup', 'business', 'photography', 'drawing', 'ui-ux', 'gaming', 'website', 'yoga', 'jazz', 'instagram', 'podcast', 'poetry', 'music', 'comedy', 'software', 'youtube', 'travel', 'art', 'coaching']

KO_FI = ['health fitness', 'art', 'comics', 'social', 'science tech', 'lifestyle', 'podcast', 'gaming', 'writing', 'animation', 'photography', 'commissions', 'design', 'advice', 'music', 'videoandfilm', 'fundraising', 'blogging', 'food drink', 'streaming', 'education', 'dance theatre', 'software', 'drawing painting', 'cosplay']

PATREON = ['podcasts', 'video', 'games', 'cosplay', 'comics', 'writing', 'music', 'adult-games', 'adult-photography', 'adult-video', 'adult-cosplay', 'animation', 'drawing-painting', 'photography', 'crafts-diy', 'dance-theater', 'magazine']

'''
writing, journalism, newsletter, magazine
education, teaching, coaching, crafts-diy
design, drawing, ui-ux, art, comics, animation
blog, blogging, instagram, youtube, lifestyle
piano, jazz, poetry, music, dance, theatre,
filmmaking, videoandfilm, streaming, video
food, drink
business
photography
gaming, games
yoga, health fitness
podcast
travel
science tech, software
'''
