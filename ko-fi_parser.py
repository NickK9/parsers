from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import SessionNotCreatedException as wrong_driver
import time
import json
from bs4 import BeautifulSoup as bs
import requests
import re

'''gold users'''

def parse_users(category):
    # browser.get('https://ko-fi.com/Home/featured?category={}'.format(category))
    req = requests.get('https://ko-fi.com/Home/featured?category={}'.format(category), headers=headers)
    soup = bs(req.text, 'html.parser')
    body = soup.find("div", class_="row ui-blog-grid")
    needed = re.findall(r'href="(.+)"', str(body))
    print('Спарсил всех из', category)
    return needed
    

def parse_accounts(user):
    # '/J3J11JLAU', '/X8X81HEXK' with / without
    req = requests.get('https://ko-fi.com/{}'.format(user))
    soup = bs(req.text, 'html.parser')
    body = soup.find("div", class_="col-xs-12 blockquote-box-mrgn")
    soc_list = body.find_all('a', href=True)
    soc_list = [i['href'] for i in soc_list if 'http' in i['href']]
    ko_fi_users[user] = soc_list
    print('спарсил все соцсети', user)

if __name__ == '__main__':
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36'
            }
    categorys = ['art']
    '''
    'cosplay', 
    'comics', 
    'writing', 
    'photography', 
    'podcast', 
    'blogging', 
    'gaming', 
    'design', 
    'advice', 
    'animation', 
    'commissions', 
    'dance+%26+theatre', 
    'drawing+%26+painting', 
    'education', 
    'food+%26+drink', 
    'fundraising', 
    'health+%26+fitness', 
    'lifestyle', 
    'money', 
    'music', 
    'science+%26+tech', 
    'social', 
    'software', 
    'streaming', 
    'video+and+film']
    '''
    for c in categorys:
        ko_fi_users = {}
        users = parse_users(c)
        for u in users:
            parse_accounts(u)
        if ('%' in c):
            c = c.replace('%', '') 
        if ('+' in c):
            c = c.replace('+', '')
        if ('26' in c):
            c = c.replace('26', ' ')
        with open("ko-fi_gold_users_{}.json".format(c), "w") as f:
            json.dump(ko_fi_users, f) 
    print('DONE')
    