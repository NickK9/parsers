from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import ElementNotInteractableException
import time
import json
from bs4 import BeautifulSoup as bs
import requests
import re
from spreadsheet import Spreadsheet


def open_all_users_on_one_page():
    show_more_button = browser.find_element_by_xpath('//*[@id="show-more-creators"]')
    print('START PARSE BUTTON USERS')
    while True:
        try:
            show_more_button.click()
            time.sleep(1)
            show_more_button = browser.find_element_by_xpath('//*[@id="show-more-creators"]')
            time.sleep(1)
        except ElementNotInteractableException:
            break
    print('DONE')

def find_all_accounts_links():
    print('START PARSE USERS PROFILES')
    users_links = []
    users_pages = browser.find_elements_by_xpath('//*[@class="creator-custom-bg br-r-8"]')
    print(len(users_pages))
    counter = 1
    for i in users_pages:
        link = i.find_elements_by_css_selector('#creators-hs-tags > div:nth-child({}) > div > a'.format(counter))
        for j in link:
            users_links.append(j.get_attribute('href'))
        counter += 1
    browser.close()
    print('DONE')
    return users_links

def parse_all_social_links():
    print('START PARSE USERS SOCIAL LINKS')
    users_with_soc_links = {}

    for j in users_links:
        soc_links = []
        req = requests.get(j)
        soup = bs(req.text, 'html.parser')
        body = soup.find_all("a", class_="abs-spread-link ctr-social-link flex flex-both-center")
        for i in body:
            soc_links.append(i['href'])
        j = j.split('https://www.buymeacoffee.com/')
        users_with_soc_links[j[1]] = soc_links
    print('DONE')
    return users_with_soc_links

def prepare_data_for_docs():
    print('START PREPARE DATA FOR DOCS')
    csv_headers = [['username', 'categories', 'buy_me_a_coffe_link', 'instagram', 'twitter', 'reddit', 'youtube', 'facebook', 'self site']]
    big_finished_list = []
    for u in users_with_soc_links:
        user_d = {'instagram':'', 'twitter':'', 'reddit':'', 'youtube':'',
            'facebook':'', 'self site':''}
        # user_d = {}
        other_links = []
        for j in users_with_soc_links[u]:
            if 'instagram' in j:
                user_d['instagram'] = j
            elif 'youtube' in j:
                user_d['youtube'] = j
            elif 'reddit' in j:
                user_d['reddit'] = j
            elif 'twitter' in j:
                user_d['twitter'] = j
            elif 'facebook' in j:
                user_d['facebook'] = j
            elif 'http' in j:
                other_links.append(j)
            else:
                pass
        u_list = [u, search_param, 'https://www.buymeacoffee.com/'+u, user_d['instagram'], user_d['twitter'], user_d['reddit'], user_d['youtube'], 
                    user_d['facebook'], ', '.join(user_d['self site'])]
        big_finished_list.append(u_list)
    csv_headers += big_finished_list
    print('DONE')
    return csv_headers, big_finished_list

def write_in_docs():
    print('START PUSHING')
    spreadsheet = Spreadsheet('1-GjTbE46XY-2tUmpeOmeQJOucGybriCzieATdpioCwM', 'BuyMeCof_Youtube!A:Q')
    spreadsheet.insert_values(csv_headers)
    print('ALL WORK DONE SUCCESS')

if __name__ == '__main__':
    search_params = ('youtube', 'instagram', 'covid-19', 'writing', 'music', 'newsletter', 'podcast', 'drawing', 'piano',
        'gaming', 'yoga', 'blog', 'education', 'comedy', 'standup', 'art', 'photography', 'business', 'website',
        'design', 'food', 'coaching', 'teaching', 'journalism', 'jazz', 'filmmaking', 'travel', 
        'software', 'ui-ux', 'poetry')
    for search_param in search_params:
        browser = webdriver.Chrome(executable_path=r".\chromedriver.exe")
        category_users = {}
        browser.get('https://www.buymeacoffee.com/explore/{}'.format(search_param))
        open_all_users_on_one_page() # с помощью кнопки show more открываем всех юзеров с тем или иным параметром
        users_links = find_all_accounts_links() # собираем ссылки на аакаунты у всех юзеров на странице
        users_with_soc_links = parse_all_social_links() # собираем ссылки на соц. сети у всех юзеров
        csv_headers, big_finished_list = prepare_data_for_docs() # подготавливаем массиф для googledocs
        # write_in_docs() # запихиваем всё в докс
        category_users[search_param] = big_finished_list
        with open("buy_me_a_coffe_{}.json".format(search_param), "w") as f:
            json.dump(category_users, f)
        print('УСПЕШНО СПАРСИЛ {}, осталось {}'.format(search_param, len(search_params)-search_params.index(search_param)))
