from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import SessionNotCreatedException as wrong_driver
import time
import json
from bs4 import BeautifulSoup as bs
import requests
import re


def take_users_patreon(category):
    '''берет линки всех креэйторов патреона'''
    ask = requests.get(category)
    big_fucking_json = json.loads(ask.content)
    links = ['https://graphtreon.com/creator/' + i['link'].split('_')[0] for i in big_fucking_json['data']]
    print('Всего {} юзеров'.format(len(links)))
    return links

def take_soc_patreon(links):
    '''берет ссылки социальных сетей из личных кабинетов'''
    for j in links:
        req = requests.get(j)
        soup = bs(req.text, 'html.parser')
        body = soup.find_all("span", class_="headerstats-header")
        users_links = []
        for i in body:
            if 'href' in str(i):
                link = i.find('a', href=True)
                users_links.append(link['href'])
        username = j.replace('https://graphtreon.com/creator/', '')
        users[username] = users_links
        print('Осталось:', 5000 - len(users))

if __name__ == '__main__':
    categories = ('https://graphtreon.com/api/creators/podcasts?_=1596994267855',
    'https://graphtreon.com/api/creators/video?_=1596994289091',
    'https://graphtreon.com/api/creators/games?_=1596994341436',
    'https://graphtreon.com/api/creators/cosplay?_=1596994352155',
    'https://graphtreon.com/api/creators/comics?_=1596994368200',
    'https://graphtreon.com/api/creators/writing?_=1596994376891',
    'https://graphtreon.com/api/creators/music?_=1596994483677',
    'https://graphtreon.com/api/creators/adult-games?_=1596994504370',
    'https://graphtreon.com/api/creators/adult-photography?_=1596994513062',
    'https://graphtreon.com/api/creators/adult-video?_=1596994521656',
    'https://graphtreon.com/api/creators/adult-cosplay?_=1596994529186',
    'https://graphtreon.com/api/creators/animation?_=1596994540326',
    'https://graphtreon.com/api/creators/drawing-painting?_=1596994552794',
    'https://graphtreon.com/api/creators/photography?_=1596994563482',
    'https://graphtreon.com/api/creators/crafts-diy?_=1596994601609',
    'https://graphtreon.com/api/creators/dance-theater?_=1596994615781',
    'https://graphtreon.com/api/creators/magazine?_=1596994632435',
    'https://graphtreon.com/api/creators/other?_=1596994669497')
    
    for category in categories:
        small_cat = category.replace('https://graphtreon.com/api/creators/', '')
        small_cat = small_cat.split('?')
        small_cat = small_cat[0]
        print('Беру {}'.format(small_cat))
        users = {}
        print('Начинаю собирать ссылки')  
        all_links = take_users_patreon(category)
        print('Успешно закончил')
        print(len(all_links))

        import threading
        # 45829 / 7 = 6547
        # 13094 / 5 (+1 в уме) = 2619
        first = all_links[5000:6000]
        second = all_links[6000:7000]
        third = all_links[7000:8000]
        four = all_links[8000:9000]
        five = all_links[9000:10000]
       # print(len(first) + len(second)) #+ len(third))
       # first1 = third[:1460]   #### 
       # first2 = third[1460:2920]   ####
       # first3 = third[2920:4380]   ####
        print(len(first) + len(second) + len(third) + len(four) + len(five))

        thread1 = threading.Thread(target=take_soc_patreon, args=(first,)) 
        thread2 = threading.Thread(target=take_soc_patreon, args=(second,)) 
        thread3 = threading.Thread(target=take_soc_patreon, args=(third,)) 
        thread4 = threading.Thread(target=take_soc_patreon, args=(four,)) 
        thread5 = threading.Thread(target=take_soc_patreon, args=(five,)) 
        
        thread1.start()
        thread2.start()
        thread3.start()
        thread4.start()
        thread5.start()

        thread1.join()
        thread2.join()
        thread3.join()
        thread4.join()
        thread5.join()

        # take_soc_patreon(all_links)
        print('Успешно закончил')
        print('Начинаю записывать в файл')
        with open("{}_patreon_users2.json".format(small_cat), "w") as f: #### 1
                json.dump(users, f) 
        print('Успешно закончил')
        print('В социальном файле:', len(users), 'ссылок')
        break
    print('Успешно обработал все категории')
