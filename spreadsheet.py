#!/usr/bin/python3.6

from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']


class Spreadsheet():
    def __init__(self, id, range):
        """Shows basic usage of the Sheets API.
        Prints values from a sample spreadsheet.
        """
        creds = None
        pickle_file = './token.pickle'
        credentials_file = './credentials.json'
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists(pickle_file):
            with open(pickle_file, 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    credentials_file, SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open(pickle_file, 'wb') as token:
                pickle.dump(creds, token)

        self.service = build('sheets', 'v4', credentials=creds)
        self.id = id
        self.range = range

    def insert_values(self, values):
        print("inserting values")
        body = {
            'values': values
        }
        result = self.service.spreadsheets().values().update(
            spreadsheetId=self.id, range=self.range,
            valueInputOption="USER_ENTERED", body=body).execute()
        print('{0} cells updated.'.format(result.get('updatedCells')))

    def clear_values(self):
        print("clear values")
        clear_values_request_body = {
            # TODO: Add desired entries to the request body.
        }
        self.service.spreadsheets().values().clear(
            spreadsheetId=self.id,
            range=self.range,
            body=clear_values_request_body
        ).execute()

    def get_values(self):
        result = self.service.spreadsheets().values().get(
            spreadsheetId=self.id,
            range=self.range
        ).execute()
        print(result)
        rows = result.get('values', [])
        return rows





