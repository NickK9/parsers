import json 
import requests
from bs4 import BeautifulSoup as bs


def find_patreon_users_links(counter):
    with open("patreon_over_10_patrons.json", "r") as f:
        CHECK = json.load(f)
        counter1 = len(CHECK)

    print('Открываю патреоновцев')
    with open("try_patrik.json", "r") as f:
        users_with_soc_links = json.load(f)
        counter2 = len(users_with_soc_links)

    CHECK = CHECK[len(users_with_soc_links):]

    users = []

    for i in CHECK:
        need = i[0].split('_&_')
        users.append(need[0])

    for user in users:
        req = requests.get('https://www.patreon.com/{}'.format(user))
        soup = bs(req.text, 'html.parser')
        user_links = soup.find_all("a", class_="sc-fzoiQi iWwqxU", href=True)
        if user_links:
            user_links = [i["href"] for i in user_links]
        print(user_links)
        users_with_soc_links[user] = user_links
        counter2 += 1
        print('Осталось', counter1 - counter2, 'из', counter1)
        with open("try_patrik.json", "w") as f:
            json.dump(users_with_soc_links, f)


if __name__ == "__main__":
    counter = 0
    while True:
        try:
            find_patreon_users_links(counter)
            break
        except Exception as e:
            print(e)
            continue
    print('DONE')
