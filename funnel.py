from bitrix24 import Bitrix24
from spreadsheet import Spreadsheet
from datetime import timedelta, date
import time


class Funnel:
    def __init__(self, users, lessons, payments, leads):
        payments_total = {}

        for payment in payments:
            customer_id = payment["customer_id"]
            income = float(payment["income"])
            # если это возврат средств, то доход будет отрицательным
            if payment["pay_type_id"] == 5:
                income = income * (-1)

            if customer_id not in payments_total:
                payments_total[customer_id] = income


                flag = True
                for user in users:
                    if user["id"] == customer_id:
                        flag = False

                if flag:
                    users.append({
                        "id": customer_id,
                        "birthday_date": payment["payment_date"]
                    })

            else:
                payments_total[customer_id] += income

        for user in users:
            user["payments_total"] = 0
            if user["id"] in payments_total:
                user["payments_total"] = payments_total[user["id"]]
            if "has_lesson" not in user:
                user["has_lesson"] = 0
            if "paid_lesson" not in user:
                user["paid_lesson"] = 0
            if "paid_lesson" not in user:
                user["paid_lesson"] = 0
            if "pass_lesson" not in user:
                user["pass_lesson"] = 0
            if "paid_lessons" not in user:
                user["paid_lessons"] = 0

            if user["payments_total"] > 0:
                user["paid_lesson"] = 1

            if user["payments_total"] > 2000:
                user["paid_lessons"] = 1

        for lesson in lessons:
            if len(lesson["customer_ids"]) == 0:
                continue
            if lesson["lesson_type_id"] != 3:
                continue
            customer_id = lesson["customer_ids"][0]

            for user in users:
                if user["id"] == customer_id:
                    user["has_lesson"] = 1

                    #if user["payments_total"] > 0:
                    #    user["paid_lesson"] = 1

                    if lesson["status"] == 3:
                        user["pass_lesson"] = 1

                    #if user["payments_total"] > 2000:
                    #    user["paid_lessons"] = 1


        values = [
            [
                "Неделя",
                "Лиды",
                "Первый контакт",
                "Второй контакт",
                "Записались",
                "Оплатили",
                "Прошли",
                "Ждут звонка",
                "Не готовы",
                "Оплачивают",
                "Отказ",
                "Купили пакет",
                "Сколько принесли",
                "Кто не купил"
            ]]

        for single_date in leads:
            #print(single_date.strftime("%d.%m.%y"))
            users_by_week = 0
            first_call = 0
            second_call = 0
            has_lesson = 0
            paid_lesson = 0
            pass_lesson = 0
            wait_call = 0
            dont_ready = 0
            paying = 0
            junk = 0

            paid_lessons = 0
            total = 0
            ids = []

            users_by_week = len(leads[single_date])

            status = 0

            for bitrix_lead in leads[single_date]:

                status = bitrix_lead["STATUS_ID"]
                if status == "NEW":
                    first_call += 1
                if status == "9":
                    second_call += 1
                if status == "6":
                    wait_call += 1
                if status == "5":
                    dont_ready += 1
                if status == "2":
                    paying += 1
                if status == "JUNK":
                    junk += 1

            for user in users:
                if user["id"] in [156, 1364, 2438, 873, 2680]:
                    continue
                user_date = user["birthday_date"]

                if 0 <= (user_date - single_date).days < 7:
                    has_lesson += user["has_lesson"]
                    paid_lesson += user["paid_lesson"]
                    pass_lesson += user["pass_lesson"]
                    paid_lessons += user["paid_lessons"]
                    total += user["payments_total"]

                    if user["pass_lesson"] > 0 and user["paid_lessons"] == 0:
                        ids.append(user["id"])


            values.append([
                single_date.strftime("%d.%m.%y"),
                users_by_week,
                first_call,
                second_call,
                has_lesson,
                paid_lesson,
                pass_lesson,
                wait_call,
                dont_ready,
                paying,
                junk,
                paid_lessons,
                total,
                str(ids)
            ])

        spreadsheet = Spreadsheet('1EIAGNtSrIHNrmevmsM-CMiJiV4gRFgyC8BBkF7ad2mI', 'воронка!A:Q')
        spreadsheet.insert_values(values)




def parsePhoneNumber(str):
    return ''.join(i for i in str if i.isdigit())