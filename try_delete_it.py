import json 
from spreadsheet import Spreadsheet

data = []
headers = ['creator name', 'link', 'patrons', 'earnings_per_month', 'at_platform_since',
    'user_category', 'twitter', 'youtube']
data.append(headers)
with open("full_top_patreon_users.json", "r") as f:
    users = json.load(f)

for i in users['data']:
    info = [i['creator_name'], 
        i['link'],
        i['patrons'],
        i['earnings_per_month'],
        i['at_platform_from'],
        i['user_category'],
        i['twitter'],
        i['youtube']]
    data.append(info)
spreadsheet = Spreadsheet('1-GjTbE46XY-2tUmpeOmeQJOucGybriCzieATdpioCwM', 'TopPatreon!A:Q')
spreadsheet.insert_values(data)