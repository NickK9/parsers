import json

with open("try_patrik.json", "r") as f:
    users_with_soc_links = json.load(f)

full = {}

for i in users_with_soc_links:
    if users_with_soc_links[i]:
        full[i] = users_with_soc_links[i]

all_links = {}

for l in full:
    needed = {}
    needed['instagram'] = ''
    needed['twitter'] = ''
    needed['facebook'] = ''
    needed['twitch'] = ''
    needed['youtube'] = ''
    other = []
    for j in full[l]:
        if 'instagram' in j:
            needed['instagram'] = j
        elif 'twitter' in j:
            needed['twitter'] = j
        elif 'facebook' in j:
            needed['facebook'] = j
        elif 'twitch' in j:
            needed['twitch'] = j
        elif 'youtube' in j:
            needed['youtube'] = j
        else:
            continue
    all_links[l] = needed

print(len(users_with_soc_links))
print(len(all_links))


#print(all_links['v0idless'])
# instagram 11280
# facebook 4680
# twitter 8717
# 10204 с левыми