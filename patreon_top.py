from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import SessionNotCreatedException as wrong_driver
import time
import json
from bs4 import BeautifulSoup as bs
import requests
import re


def take_users_patreon(category):
    '''берет линки всех креэйторов патреона'''
    ask = requests.get(category)
    big_fucking_json = json.loads(ask.content)
    big_fucking_data = []
    counter = 0
    how_much_counter = 0
    for creator in big_fucking_json['data']:
        creator_name = creator['link'].split('&')[0]
        creator_name = creator_name[0:-1]
        link = 'https://graphtreon.com/creator/' + creator_name
        patrons = creator["patrons"]
        earnings_per_month = creator["earnings"]
        at_platform_from = creator["patreonPublishedAt"]
        req = requests.get(link)
        soup = bs(req.text, 'html.parser')
        try:
            user_category = soup.find("div", class_="box-footer").h4.text
        except AttributeError:
            counter += 1
            user_category = 'MISS'
        body = soup.find_all("span", class_="headerstats-header")
        users_links = []
        for i in body:
            if 'href' in str(i):
                link_soc = i.find('a', href=True)
                users_links.append(link_soc['href'])
        twitter = ''
        youtube = ''
        for l in users_links:
            if "twitter" in l:
                twitter = l
            if "youtube" in l:
                youtube = l
        user_dict = {"creator_name":creator_name,
            "link":link,
            "patrons":patrons,
            "earnings_per_month":earnings_per_month,
            "at_platform_from": at_platform_from,
            "user_category": user_category,
            "twitter": twitter,
            "youtube": youtube}
        how_much_counter += 1
        print(how_much_counter, 'of', len(big_fucking_json["data"]), 'with', counter, 'mistakes' )
        big_fucking_data.append(user_dict)
    print('Всего {} юзеров'.format(len(big_fucking_data)))
    return big_fucking_data


if __name__ == '__main__':
    category = 'https://graphtreon.com/api/creators/?_=1597243857664'
    lots_of_fucking_data = {}
    links = take_users_patreon(category)
    data = {"data":links}
    with open("full_top_patreon_users.json", "w") as f: #### 1
        json.dump(data, f) 
    print('Успешно закончил')
