import time
import json
from bs4 import BeautifulSoup as bs
import requests
import re
from spreadsheet import Spreadsheet
import psycopg2


class EmptyList(Exception):
    pass

def big_parse():
    with open("patreon_tail_8329.json", "r") as f:
        CHECK = json.load(f)


    conn = psycopg2.connect(dbname='postgres', user='postgres', 
                                    password='simplyclever343', 
                                    host='localhost',
                                    port=5432)
    curs = conn.cursor()

    curs.execute('SELECT MAX(id) FROM patreon_users')
    last_id = curs.fetchone()[0]
    # CHECK = CHECK[last_id:]
    checker = last_id

    if last_id == 65578:
        raise EmptyList

    data = []
    headers = ['creator_name', 'patreon_link', 'patrons', 'earnings_per_month', 'at_platform_from', 'category', 'twitter', 'youtube']
    big_fucking_data = []
    how_much_counter = 0
    counter = 0
    update_counter = 0
    for creator in CHECK:
        s1 = time.time()
        creator_name = creator[0].split('&')[0]
        creator_name = creator_name[0:-1]
        link = 'https://graphtreon.com/creator/' + creator_name
        patrons = creator[1]
        earnings_per_month = creator[2]
        at_platform_from = creator[4]
        req = requests.get(link)
        soup = bs(req.text, 'html.parser')
        try:
            user_category = soup.find("div", class_="box-footer").h4.text
        except AttributeError:
            counter += 1
            user_category = 'MISS'
        body = soup.find_all("span", class_="headerstats-header")
        users_links = []
        for i in body:
            if 'href' in str(i):
                link_soc = i.find('a', href=True)
                users_links.append(link_soc['href'])
        twitter = ''
        youtube = ''
        for l in users_links:
            if "twitter" in l:
                twitter = l
            if "youtube" in l:
                youtube = l
            user_list = [creator_name,
                link,
                patrons,
                earnings_per_month,
                at_platform_from,
                user_category,
                twitter,
                youtube]
        curs.execute("INSERT INTO patreon_users(creator_name, link, patrons, earnings_per_month, at_platform_from, user_category, twitter, youtube) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (creator_name, link, patrons, earnings_per_month, at_platform_from, user_category, twitter, youtube))
        conn.commit()
        how_much_counter += 1
        last_id += 1
        s2 = time.time()
        print(how_much_counter, 'of', len(CHECK), 'with', counter, 'mistakes' )
        times = s2-s1
        print('Времени осталось:', (times * (len(CHECK) - how_much_counter)) / 60, 'минут')
    print('Всего {} юзеров'.format(len(big_fucking_data)))
    print(len(big_fucking_data))

    conn.close()
    print('ГОТОВО')

if __name__ == "__main__":
    while True:
        try:
            big_parse()
        except EmptyList as e:
            break
        except:
            print('')
            print('ERROR, made a mistake, RELOAD')
            print('')
            break
            time.sleep(420) # 7 минут
            # continue
    print('ГОТОВО')

# 12711 of 57186 with 1 mistakes
# потерял, так как айди с 8393, потеряно 8393 объекта
# 1 of 36083 with 0 mistakes
# 17358 of 17358 with 1 mistakes
# должно быть 65578